The index.php file of the project is located in the public directory (that is where server should point to)

Please run `composer install` before running the app.

Tests can be run `php vendor/bin/phpunit tests`

_And remember, this is a test assignment :)._

Also, about the task specification, "all files must be PSR-4 compliant" - PSR-4 is an autoloading standart.

"Must be written with full OOP parameters" -> That is a confusing sentence. 

"You must apply DocBlocks to all..." - No, it is a bad practice. 
DocBlocks are only required to describe parts of code that can't be written in PHP. (ex. SomeClass[])
