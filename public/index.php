<?php

declare(strict_types=1);

require_once '../vendor/autoload.php';

use ContactApp\Builder\AddressBuilder;
use ContactApp\Builder\ContactBuilder;
use ContactApp\Model\Book;
use ContactApp\Model\View;
use ContactApp\Service\ViewRenderingService;

$contactBuilder = new ContactBuilder();
$addressBuilder = new AddressBuilder();

$contacts = [];

# Add first contact to list of contacts
$contacts[] = $contactBuilder->build([
    'name' => 'Mr John Doe',
    'email' => 'john@doe.com',
]);


# Add second contact to list of contacts
$contacts[] = $contactBuilder->build([
    'name' => 'Ms Anna Baker',
    'email' => 'anna@baker.com',
]);
# Open new book
$book = new Book();

# Add first address with both contacts
$book->addAddress(
    $addressBuilder->build([
        'house_number' => '33',
        'street' => 'Market street',
        'city' => 'London',
        'postcode' => 'EC4 MB5',
        'county' => 'Greater London',
        'country' => 'GB',
        'contacts' => $contacts,
    ])
);

# Reset contact list
$contacts = [];

# Add first contact to list of contacts
$contacts[] = $contactBuilder->build([
    'name' => 'Ms Dane Rovens',
    'email' => 'dane@rovens.com',
]);

# Add second address with one contact
$book->addAddress(
    $addressBuilder->build([
        'house_number' => '22',
        'street' => 'Tower street',
        'postcode' => 'SK4 1HV',
        'country' => 'GB',
        'contacts' => $contacts,
    ])
);

$view = new View('contact-list', [
    'contactBook' => $book,
]);

# Output all of the known information

$renderer = new ViewRenderingService();

echo $renderer->render($view);
exit;

# preview of expected output below
/**
 *
 * Book record: #1
 * Address: 33 Market street, London, Greater London, EC4 MB5, GB
 * Contact #1: <john@doe.com> John Doe
 * Contact #2: <anna@baker.com> Anna Baker
 * Book record: #2
 * Address: 22 Tower Street, SK4 1HV, GB
 * Contact #1: <dane@rovens.com> Ms Dane Rovens
 **/