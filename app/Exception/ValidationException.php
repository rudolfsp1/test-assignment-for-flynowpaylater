<?php

declare(strict_types=1);

namespace ContactApp\Exception;

use InvalidArgumentException;

class ValidationException extends InvalidArgumentException
{
    public static function throw(string $message): void
    {
        throw new static($message);
    }
}