<?php

declare(strict_types=1);

namespace ContactApp\Model;

class Book
{
    /**
     * @var Address[]
     */
    protected $records = [];

    public function __construct(array $records = [])
    {
        $this->records = $records;
    }

    public function addAddress(Address $address): void
    {
        $this->records[spl_object_hash($address)] = $address;
    }

    public function removeAddress(Address $address): void
    {
        unset($this->records[spl_object_hash($address)]);
    }

    /**
     * @return Address[]
     */
    public function getAddresses(): array
    {
        return $this->records;
    }
}