<?php

declare(strict_types=1);

namespace ContactApp\Model;

class Address
{
    /**
     * @var string
     */
    protected $houseNumber;

    /**
     * @var string
     */
    protected $street;

    /**
     * @var string
     */
    protected $city;

    /**
     * @var string
     */
    protected $county;

    /**
     * @var string
     */
    protected $postcode;

    /**
     * @var string
     */
    protected $country;

    /**
     * @var Contact[]
     */
    protected $contacts;

    /**
     * @param string $houseNumber
     * @param string $street
     * @param string $city
     * @param string $county
     * @param string $postcode
     * @param string $country
     * @param Contact[] $contacts
     */
    public function __construct(
        string $houseNumber,
        string $street,
        string $city,
        string $county,
        string $postcode,
        string $country,
        array $contacts = []
    ) {
        $this->houseNumber = $houseNumber;
        $this->street = $street;
        $this->city = $city;
        $this->county = $county;
        $this->postcode = $postcode;
        $this->country = $country;
        $this->contacts = $contacts;
    }

    public function __toString(): string
    {
        $addressParts = [];

        $addressParts[] = ($this->houseNumber . ' ' . $this->street) ?: null;
        $addressParts[] = $this->city ?: null;
        $addressParts[] = $this->county ?: null;
        $addressParts[] = $this->postcode ?: null;
        $addressParts[] = $this->country ?: null;

        $addressParts = array_filter($addressParts);

        return implode(', ', $addressParts);
    }

    public function getHouseNumber(): string
    {
        return $this->houseNumber;
    }

    public function setHouseNumber(string $houseNumber): void
    {
        $this->houseNumber = $houseNumber;
    }

    public function getStreet(): string
    {
        return $this->street;
    }

    public function setStreet(string $street): void
    {
        $this->street = $street;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function setCity(string $city): void
    {
        $this->city = $city;
    }

    public function getCounty(): string
    {
        return $this->county;
    }

    public function setCounty(string $county): void
    {
        $this->county = $county;
    }

    public function getPostcode(): string
    {
        return $this->postcode;
    }

    public function setPostcode(string $postcode): void
    {
        $this->postcode = $postcode;
    }

    public function getCountry(): string
    {
        return $this->country;
    }

    public function setCountry(string $country): void
    {
        $this->country = $country;
    }

    /**
     * @return Contact[]
     */
    public function getContacts(): array
    {
        return $this->contacts;
    }

    public function hasContacts(): bool
    {
        return !empty($this->contacts);
    }

    /**
     * @param Contact[] $contacts
     */
    public function setContacts(array $contacts): void
    {
        $this->contacts = $contacts;
    }
}