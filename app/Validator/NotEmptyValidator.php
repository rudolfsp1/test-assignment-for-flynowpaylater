<?php

declare(strict_types=1);

namespace ContactApp\Validator;

use ContactApp\Exception\ValidationException;

final class NotEmptyValidator implements ValidatorInterface
{
    private const ILLEGAL_VALUES = ['', [], null];

    public function validate(string $field, $value)
    {
        if (in_array($value, self::ILLEGAL_VALUES, true)) {
            $this->fail($field);
        }

        return $value;
    }

    protected function fail(string $field): void
    {
        ValidationException::throw($field . ' is required');
    }
}