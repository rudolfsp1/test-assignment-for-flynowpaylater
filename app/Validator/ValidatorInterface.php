<?php

declare(strict_types=1);

namespace ContactApp\Validator;

interface ValidatorInterface
{
    public function validate(string $field, $value);
}