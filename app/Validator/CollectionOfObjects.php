<?php

declare(strict_types=1);

namespace ContactApp\Validator;

use ContactApp\Exception\ValidationException;

final class CollectionOfObjects implements ValidatorInterface
{
    public function validate(string $field, $value)
    {
        if (!is_iterable($value)) {
            $this->fail($field);
        }

        foreach ($value as $object) {
            if (!is_object($object)) {
                $this->fail($field);
            }
        }

        return $value;
    }

    protected function fail(string $field): void
    {
        ValidationException::throw($field . ' must be iterable and contain only objects');
    }
}