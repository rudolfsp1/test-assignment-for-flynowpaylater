<?php

declare(strict_types=1);

namespace ContactApp\Validator;

use ContactApp\Exception\ValidationException;

final class EmailValidator implements ValidatorInterface
{
    public function validate(string $field, $value)
    {
        if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
            $this->fail($value);
        }

        return $value;
    }

    protected function fail(string $value): void
    {
        ValidationException::throw($value . ' is not a valid email');
    }
}