<?php

declare(strict_types=1);

namespace ContactApp\Builder;

use ContactApp\Model\Contact;
use ContactApp\Validator\EmailValidator;
use ContactApp\Validator\NotEmptyValidator;

final class ContactBuilder
{
    public function build(array $payload): Contact
    {
        $notEmpty = new NotEmptyValidator();
        $isEmail = new EmailValidator();

        $email = $payload['email'] ?? '';

        return new Contact(
            $notEmpty->validate('name', $payload['name'] ?? ''),
            $isEmail->validate('email',
                $notEmpty->validate('email', $email)
            )
        );
    }
}
