<?php

declare(strict_types=1);

namespace ContactApp\Builder;

use ContactApp\Model\Address;
use ContactApp\Validator\CollectionOfObjects;
use ContactApp\Validator\NotEmptyValidator;

final class AddressBuilder
{
    public function build(array $payload): Address
    {
        $notEmpty = new NotEmptyValidator();
        $collectionOfObjects = new CollectionOfObjects();

        return new Address(
            $payload['house_number'] ?? '',
            $notEmpty->validate('street', $payload['street'] ?? ''),
            $payload['city'] ?? '',
            $payload['county'] ?? '',
            $notEmpty->validate('postcode', $payload['postcode'] ?? ''),
            $notEmpty->validate('country', $payload['country'] ?? ''),
            $collectionOfObjects->validate('contacts', $payload['contacts'] ?? [])
        );
    }
}
