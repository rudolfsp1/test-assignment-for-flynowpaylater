<?php

declare(strict_types=1);

namespace ContactApp\Service;

use ContactApp\Model\View;

class ViewRenderingService
{
    /**
     * @var array
     */
    protected $config = [
        'view_directory' => __DIR__ . '/../../views',
        'view_extension' => 'phtml',
    ];

    public function __construct(array $config = [])
    {
        $this->config = array_merge($this->config, $config);
    }

    public function render(View $view): string
    {
        $templateFile = sprintf('%s/%s.%s',
            $this->config['view_directory'],
            $view->getTemplate(),
            $this->config['view_extension']
        );

        return $this->isolatedRequire($view->getVariables(), $templateFile);
    }

    /**
     * @desc Require inside this method to prevent variables overwriting current scope
     * @param array $__vars
     * @param string $__file
     * @return string
     */
    protected function isolatedRequire(array $__vars, string $__file): string
    {
        ob_start();

        extract($__vars, EXTR_OVERWRITE);

        require $__file;

        return (string)ob_get_clean();
    }
}