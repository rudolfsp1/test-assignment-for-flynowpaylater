<?php

declare(strict_types=1);

namespace ContactApp\Tests;

use ContactApp\Builder\ContactBuilder;
use ContactApp\Exception\ValidationException;
use PHPUnit\Framework\TestCase;

class ContactBuilderTest extends TestCase
{
    /**
     * @var ContactBuilder
     */
    protected $builder;

    protected function setUp(): void
    {
        $this->builder = new ContactBuilder();
    }

    public function test_it_validates_the_payload(): void
    {
        $this->expectException(ValidationException::class);

        $this->builder->build([
            'name' => 'John Doe',
            'email' => 'not real email',
        ]);
    }

    public function test_it_can_create_a_contact(): void
    {
        $this->assertNotEmpty($this->builder->build([
            'name' => 'John Doe',
            'email' => 'real@email.com',
        ]));
    }
}