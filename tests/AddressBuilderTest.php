<?php

declare(strict_types=1);

namespace ContactApp\Tests;

use ContactApp\Builder\AddressBuilder;
use ContactApp\Exception\ValidationException;
use PHPUnit\Framework\TestCase;

class AddressBuilderTest extends TestCase
{
    /**
     * @var AddressBuilder
     */
    protected $builder;

    protected function setUp(): void
    {
        $this->builder = new AddressBuilder();
    }

    public function test_it_validates_the_payload(): void
    {
        $this->expectException(ValidationException::class);

        $this->builder->build([
            'i_like_trains' => false,
        ]);
    }

    public function test_it_can_create_an_address(): void
    {
        $this->assertNotEmpty($this->builder->build([
            'house_number' => '3',
            'street' => 'Salmon bakery',
            'city' => 'Salmonia',
            'county' => 'Small Pickled',
            'postcode' => 'PICK-3949',
            'country' => 'Fishia',
        ]));
    }
}